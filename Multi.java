public class Multi extends Thread
{  
    static int sum = 0;
    int ip[] = new int[2];
    int w[] = new int[2];
    int x;
    Multi(int ip[],int w[])
    {
        for(int i=0;i<2;i++){
            this.ip[i]=ip[i];
        }
        for(int i=0;i<2;i++){
            this.w[i]=w[i];
        }
	x=0;
    }
    public void run()
    {  
        for(int i=0;i<2;i++)
        {
            x=x+(ip[i]*w[i]);
            
        }
	sum +=x;
        System.out.println("Output of Neuron"+Thread.currentThread().getId()+":"+x);
    }  
   
    public static void main(String args[])
    {  
        int ip[]={150,200};
        int wn1[]={10,30};
        int wn2[]={20,40};
        int x1,x2;
        Multi t1=new Multi(ip,wn1);
        Multi t2=new Multi(ip,wn2);
        t1.start();
        t2.start();
	while(true){
		if(!(t1.isAlive() && t2.isAlive()))
		{
			System.out.println("\nOutput of both Neurons is: "+sum);
			break;
		}
              }
    }  
} 